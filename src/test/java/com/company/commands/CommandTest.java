package com.company.commands;

import com.company.factory.Factory;
import junit.framework.TestCase;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import static org.junit.jupiter.api.Assertions.*;

class CommandTest extends TestCase {

    private static Factory factory;

    static {
        try {
            factory = new Factory("factory.ini");
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getLocalizedMessage());
            System.out.println("Abort process due to uninitialized factory");
        }
    }

    @org.junit.jupiter.api.Test
    void pushExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5")));
        command.execute(context);
        assertEquals(5.0, context.getStack().get(context.getStack().size() - 1));
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5.0")));
        command.execute(context);
        assertEquals(5.0, context.getStack().get(context.getStack().size() - 1));
        command.setArgumentList(new ArrayList<>(Collections.singletonList("-5")));
        command.execute(context);
        assertEquals(-5.0, context.getStack().get(context.getStack().size() - 1));
    }

    @org.junit.jupiter.api.Test
    void popExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5")));
        command.execute(context);
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5.0")));
        command.execute(context);
        command.setArgumentList(new ArrayList<>(Collections.singletonList("-5")));
        command.execute(context);

        int initialSize = context.getStack().size();
        command = factory.getInstanceFor("pop");
        command.execute(context);
        assertEquals(initialSize - 1, context.getStack().size());
        initialSize = context.getStack().size();
        command = factory.getInstanceFor("pop");
        command.execute(context);
        assertEquals(initialSize - 1, context.getStack().size());
        initialSize = context.getStack().size();
        command = factory.getInstanceFor("pop");
        command.execute(context);
        assertEquals(initialSize - 1, context.getStack().size());
    }

    @org.junit.jupiter.api.Test
    void additionExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5")));
        command.execute(context);
        command.setArgumentList(new ArrayList<>(Collections.singletonList("6")));
        command.execute(context);
        command = factory.getInstanceFor("+");
        command.execute(context);
        assertEquals(11.0, context.getStack().get(context.getStack().size() - 1));
    }

    @org.junit.jupiter.api.Test
    void multiplyExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5")));
        command.execute(context);
        command.setArgumentList(new ArrayList<>(Collections.singletonList("6")));
        command.execute(context);
        command = factory.getInstanceFor("*");
        command.execute(context);
        assertEquals(30.0, context.getStack().get(context.getStack().size() - 1));
    }

    @org.junit.jupiter.api.Test
    void divideExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("5")));
        command.execute(context);
        command.setArgumentList(new ArrayList<>(Collections.singletonList("6")));
        command.execute(context);
        command = factory.getInstanceFor("/");
        command.execute(context);
        assertEquals(1.2, context.getStack().get(context.getStack().size() - 1));
    }

    @org.junit.jupiter.api.Test
    void sqrtExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("9")));
        command.execute(context);
        command = factory.getInstanceFor("sqrt");
        command.execute(context);
        assertEquals(3.0, context.getStack().get(context.getStack().size() - 1));
    }

    @org.junit.jupiter.api.Test
    void defineExecute() throws Exception {
        Context context = new Context();
        Command command = factory.getInstanceFor("define");
        command.setArgumentList(new ArrayList<>(Arrays.asList("pi","3.14")));
        command.execute(context);
        command = factory.getInstanceFor("push");
        command.setArgumentList(new ArrayList<>(Collections.singletonList("pi")));
        command.execute(context);
        assertEquals(3.14, context.getStack().get(context.getStack().size() - 1));
    }
}