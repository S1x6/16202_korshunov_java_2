package com.company.exception;

public class NotDefinedException extends CalculationException {
    public NotDefinedException() { super(); }
    public NotDefinedException(String message) { super(message); }
    public NotDefinedException(String message, Throwable cause) { super(message, cause); }
    public NotDefinedException(Throwable cause) { super(cause); }
}
