package com.company.exception;

public class StackException extends CalculationException {
    public StackException() { super(); }
    public StackException(String message) { super(message); }
    public StackException(String message, Throwable cause) { super(message, cause); }
    public StackException(Throwable cause) { super(cause); }
}
