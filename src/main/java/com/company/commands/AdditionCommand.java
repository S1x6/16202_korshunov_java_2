package com.company.commands;

import com.company.exception.CalculationException;
import com.company.exception.StackException;

import java.util.ArrayList;
import java.util.logging.Logger;

public class AdditionCommand extends Command {
    @Override
    public void execute(Context context) throws CalculationException {
        ArrayList<Double> stack = context.getStack();
        if (stack.size() >= 2) {
            int size = stack.size() - 1;
            Double a = stack.get(size);
            stack.remove(size);
            Double b = stack.get(size - 1);
            stack.remove(size - 1);
            stack.add(a+b);
            Logger log = Logger.getLogger(AdditionCommand.class.getName());
            log.info("Added " + a + " to " + b + ", result = " + context.getStack().get(context.getStack().size()-1) + ". Stack size: " + context.getStack().size());
        } else {
            throw new StackException("Not enough arguments on stack");
        }
    }
}
