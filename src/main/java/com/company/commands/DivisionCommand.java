package com.company.commands;

import com.company.exception.CalculationException;
import com.company.exception.StackException;

import java.util.ArrayList;
import java.util.logging.Logger;

public class DivisionCommand extends Command {
    @Override
    public void execute(Context context) throws CalculationException {
        ArrayList<Double> stack = context.getStack();
        if (stack.size() >= 2) {
            int size = stack.size() - 1;
            Double a = stack.get(size);
            stack.remove(size);
            Double b = stack.get(size - 1);
            stack.remove(size - 1);
            if (b == 0) {
                throw new CalculationException("Division by zero");
            }
            stack.add(a/b);
            Logger log = Logger.getLogger(DivisionCommand.class.getName());
            log.info("Divided " + a + " by " + b + ", result = " + context.getStack().get(context.getStack().size()-1) + ". Stack size: " + context.getStack().size());
        } else {
            throw new StackException("Not enough arguments on stack");
        }
    }
}
