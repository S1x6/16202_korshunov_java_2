package com.company.commands;

import java.util.ArrayList;
import java.util.HashMap;

public class Context {
    private ArrayList<Double> stack;
    private HashMap<String, Double> defineMap;

    public Context() {
        stack = new ArrayList<>(10);
        defineMap = new HashMap<>(10);
    }

    public ArrayList<Double> getStack() {
        return stack;
    }

    public HashMap<String, Double> getDefineMap() {
        return defineMap;
    }
}
