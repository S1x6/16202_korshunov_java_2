package com.company.commands;

import com.company.exception.CalculationException;
import com.company.exception.StackException;

import java.util.logging.Logger;

public class PopCommand extends Command {

    @Override
    public void execute(Context context) throws CalculationException {
        if (!context.getStack().isEmpty()) {
            double oldValue = context.getStack().get(context.getStack().size()-1);
            context.getStack().remove(context.getStack().size()-1);
            Logger log = Logger.getLogger(PopCommand.class.getName());
            log.info("Popped " + oldValue + ". Stack size: " + context.getStack().size());
        } else {
            throw new StackException("Stack was empty when pop");
        }
    }
}
