package com.company.commands;

import com.company.exception.CalculationException;
import com.company.exception.NotDefinedException;

import java.util.logging.Logger;

public class PushCommand extends Command {

    @Override
    public void execute(Context context) throws CalculationException {
        if (getArgumentList().size() == 1) {
            String s_argument = getArgumentList().get(0).toString();
            Double argument;
            try {
                argument = Double.valueOf(s_argument);
            } catch (NumberFormatException ex){
                argument = context.getDefineMap().get(s_argument);
                if (argument == null) {
                    throw new NotDefinedException(s_argument + " was not defined");
                }
            }

            context.getStack().add(argument);
            Logger log = Logger.getLogger(PopCommand.class.getName());
            log.info("Pushed " + argument + ". Stack size: " + context.getStack().size());
        } else {
            throw new IllegalArgumentException("Wrong amount of arguments in push command");
        }
    }
}
