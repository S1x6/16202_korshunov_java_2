package com.company.commands;

import com.company.exception.CalculationException;
import com.company.exception.StackException;

import java.util.ArrayList;
import java.util.logging.Logger;

public class SqrtCommand extends Command {
    @Override
    public void execute(Context context) throws CalculationException {
        ArrayList<Double> stack = context.getStack();
        if (stack.size() >= 1) {
            int size = stack.size() - 1;
            Double a = stack.get(size);
            stack.remove(size);
            if (a < 0) {
                throw new CalculationException("Root of negative value");
            }
            stack.add(Math.sqrt(a));
            Logger log = Logger.getLogger(SqrtCommand.class.getName());
            log.info("Square root of " + a + ", result = " + context.getStack().get(context.getStack().size()-1) + ". Stack size: " + context.getStack().size());
        } else {
            throw new StackException("Not enough arguments on stack");
        }
    }
}
