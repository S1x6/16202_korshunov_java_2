package com.company.commands;

import com.company.exception.CalculationException;

import java.util.ArrayList;

public abstract class Command {
    private ArrayList<Object> argumentList;

    public abstract void execute(Context context) throws CalculationException;

    public final void setArgumentList(ArrayList<Object> argumentList) {
        this.argumentList = argumentList;
    }

    final ArrayList<Object> getArgumentList() {
        return argumentList;
    }
}
