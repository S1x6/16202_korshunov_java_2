package com.company.commands;



import java.util.logging.Logger;

public class DefineCommand extends Command {
    @Override
    public void execute(Context context) throws IllegalArgumentException {
        if (getArgumentList().size() == 2) {
            String s_argument = getArgumentList().get(1).toString();
            Double argument;
            try {
                argument = Double.valueOf(s_argument);
            } catch (NumberFormatException ex) {
                throw new IllegalArgumentException("Wrong type of argument");
            }
            context.getDefineMap().put(getArgumentList().get(0).toString(),argument);
            Logger log = Logger.getLogger(DefineCommand.class.getName());
            log.info("Defined " + argument + " as " + getArgumentList().get(0).toString() + ". Stack size: " + context.getStack().size());
        } else {
            throw new IllegalArgumentException("Wrong amount of arguments in define command");
        }
    }
}
