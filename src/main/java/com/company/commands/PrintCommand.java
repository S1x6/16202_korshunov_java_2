package com.company.commands;

import com.company.exception.CalculationException;
import com.company.exception.StackException;

import java.util.logging.Logger;

public class PrintCommand extends Command {
    @Override
    public void execute(Context context) throws CalculationException {
        if (!context.getStack().isEmpty()) {
            System.out.println(context.getStack().get(context.getStack().size()-1));
            Logger log = Logger.getLogger(PrintCommand.class.getName());
            log.info("Printed " + context.getStack().get(context.getStack().size()-1) +". Stack size: " + context.getStack().size());
        } else {
            throw new StackException("Stack was empty when print");
        }
    }
}
