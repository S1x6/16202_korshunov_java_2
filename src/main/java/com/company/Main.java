package com.company;

import com.company.commands.Command;
import com.company.commands.Context;
import com.company.exception.CalculationException;
import com.company.factory.Factory;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Factory factory;
        try {
            factory = new Factory("factory.ini");
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println(ex.getLocalizedMessage());
            System.out.println("Abort process due to uninitialized factory");
            return;
        }

        InputStream inputStream;
        if (args.length == 1) {
            try {
                inputStream = new FileInputStream(args[0]);
            } catch (FileNotFoundException ex) {
                System.out.println(ex.getLocalizedMessage() + ". System input stream will be used instead");
                inputStream = System.in;
            }
        } else {
            inputStream = System.in;
        }

        Context context = new Context();
        String input;
        Scanner scanner = new Scanner(inputStream);
        while (scanner.hasNext()) {
            input = scanner.nextLine();
            if (input.charAt(0) == '#')
                continue;
            String[] strings = input.split("\\s");
            if (strings.length != 0) {
                Command command = factory.getInstanceFor(strings[0]);
                if (command == null) continue;
                ArrayList<Object> arguments = new ArrayList<>(Arrays.asList(strings));
                arguments.remove(0);
                command.setArgumentList(arguments);
                try {
                    command.execute(context);
                } catch (CalculationException e) {
                    System.out.println(e.getMessage());
                }
            }
        }

    }
}
