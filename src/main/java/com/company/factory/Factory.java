package com.company.factory;

import com.company.commands.Command;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

public class Factory {
    private HashMap<String, Class> map;

    public Factory(String initializationFileName) throws IOException, ClassNotFoundException {
        map = new HashMap<>(10);

        Scanner scanner = new Scanner(new File(initializationFileName));
        while (scanner.hasNext()) {
            String key = scanner.next();
            if (scanner.hasNext()) {
                map.put(key, Class.forName(scanner.next()));
            } else {
                throw new IOException("Wrong format of ini file");
            }
        }
    }

    public Command getInstanceFor(String key){
        try {
            return (Command) map.get(key).newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
            return null;
        } catch (NullPointerException ex) {
            System.out.println("No such command");
            return null;
        }
    }


}
